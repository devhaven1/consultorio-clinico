import Acciones.*;
import Personas.Doctor;

import java.util.Locale;
import java.util.Scanner;

public class Sistema{
    public static void main(String[] args) {
        start();
    }

    public static void start(){
        while (true){
            StringBuilder sb = new StringBuilder("==== Consultorio Medico ====\n");
            sb.append("Que accion deseas realizar?\n");
            sb.append("1) Dar de alta Doctor\n" +
                    "2) Dar de alta paciente\n3) Crear cita\n4) Generar reporte de Medicos\n" +
                    "5) Generar reporte de pacientes\n6) Generar reporte de citas\n" +
                    "7) Salir");
            System.out.println(sb);
            Scanner myObj = new Scanner(System.in);
            System.out.println("====================\n");
            String option = myObj.nextLine();
            System.out.println("Seleccionaste la opcion: " + option );
            if (option.equals("7")){
                break;
            }
            switch(option){
                case "1":
                    Altas.crearDoctor();
                    break;
                case "2":
                    Altas.crearPaciente();
                    break;
                case "3":
                    Altas.generarCita();
                    break;
                case "4":
                    Altas.generarReporteDoc();
                    break;
                case "5":
                    Altas.generarReportePac();
                    break;
                case "6":
                    Altas.generarReporteCitas();
                    break;
                default:
                    break;
            }
        }

    }

}
