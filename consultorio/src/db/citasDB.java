package db;

import java.util.HashMap;
import java.util.Map;

public class citasDB {
    Map<Double,String> citas = new HashMap<Double,String>();
    String filepath = "consultorio/src/reportes/citas.csv";
    public void agregar(Double id, String fecha){
        citas.put(id,fecha);
    }

    public void generarReporteDeCitas(){
        ReporGen.generateReport(filepath, citas, "Citas");
    }
}
