package db;

import java.util.HashMap;
import java.util.Map;

public class pacientesDB {
    Map<Double,String> pacientes = new HashMap<Double,String>();
    String filepath = "consultorio/src/reportes/pacientes.csv";
    public void agregar(Double id, String nombre){
        pacientes.put(id,nombre);
    }

    public void generarReporteDePacientes(){
        ReporGen.generateReport(filepath, pacientes, "Pacientes");
    }

    public Map<Double, String> getPacientes() {
        return pacientes;
    }
}
