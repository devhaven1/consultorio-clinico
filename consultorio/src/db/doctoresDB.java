package db;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class doctoresDB {
    Map<Double,String> doctores = new HashMap<Double,String>();
    String filepath = "consultorio/src/reportes/doctores.csv";
    public void agregar(Double id, String nombreespecialidad){
        doctores.put(id,nombreespecialidad);
    }

    public void generarReporteDeMedicos(){
        ReporGen.generateReport(filepath, doctores, "Doctores");
    }

    public Map<Double, String> getDoctores() {
        return doctores;
    }
}
