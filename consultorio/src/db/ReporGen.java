package db;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class ReporGen {
    public static void generateReport(String filepath,  Map<Double,String> entries, String tipo){
        try {
            FileWriter myWriter = new FileWriter(filepath);
            for(Map.Entry<Double, String> entry: entries.entrySet()){
                myWriter.write(String.format("%s,%s\n",entry.getKey(),entry.getValue()));
            }
            myWriter.close();
            System.out.println("Reporte de " + tipo +  " generado ");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
