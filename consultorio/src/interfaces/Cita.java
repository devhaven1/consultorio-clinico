package interfaces;

import Personas.Doctor;
import Personas.Paciente;

public interface Cita {
    void Agendar(Double id, String fecha, String motivo);
}
