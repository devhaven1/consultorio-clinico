package Acciones;

import Personas.Doctor;
import Personas.Paciente;
import db.citasDB;
import db.doctoresDB;
import db.pacientesDB;

import java.util.Map;
import java.util.Scanner;

public class Altas {

    static doctoresDB docb = new doctoresDB();
    static pacientesDB pdb  = new pacientesDB();
    static citasDB cdb = new citasDB();


    public static void crearDoctor(){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Cual es el nombre completo del doctor");
        String nombre = myObj.nextLine();
        System.out.println("Cual es la especialidad del doctor");
        String especialidad = myObj.nextLine();
        Doctor doc = new Doctor();
        doc.setId(Math.random() * 1000);
        doc.setNombre(nombre);
        doc.setEspecialidad(especialidad);

        // Agregar
        docb.agregar(doc.getId(), (doc.getNombre() + "," + doc.getEspecialidad()));

    }

    public static void crearPaciente(){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Cual es el nombre completo del paciente");
        String nombre = myObj.nextLine();

        Paciente pc = new Paciente();
        pc.setId(Math.random() * 1000);
        pc.setNombre(nombre);

        // Agregar
        pdb.agregar(pc.getId(), pc.getNombre());
    }

    public static void generarCita(){
        Double id = Math.random() * 100;
        Scanner myObj = new Scanner(System.in);
        System.out.println("Cual es el nombre completo del paciente");
        String nombre = myObj.nextLine();
        System.out.println("Cual es el nombre del doctor");
        String doctor = myObj.nextLine();
        for(Map.Entry<Double, String> entry: pdb.getPacientes().entrySet()){
            if (entry.getValue().contains(nombre)){
                nombre = entry.getValue();
                break;
            }
        }
        for(Map.Entry<Double, String> entry: docb.getDoctores().entrySet()){
            if (entry.getValue().contains(doctor)){
                doctor = entry.getValue();
                break;
            }
        }
        System.out.println("Fecha y hora de la cita en este formato dd/mm/yyyy 16:00");
        String date = myObj.nextLine();
        System.out.println("Motivo de la cita");
        String motivo = myObj.nextLine();
        cdb.agregar(id, nombre + "," + doctor + "," + date + "," + motivo);

    }

    public static void generarReporteDoc(){
        docb.generarReporteDeMedicos();
    }

    public static void generarReportePac(){
        pdb.generarReporteDePacientes();
    }

    public static void generarReporteCitas(){
        cdb.generarReporteDeCitas();
    }
}
